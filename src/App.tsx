import React from 'react';
import { Route, Routes } from "react-router-dom";

import SideBar from './components/SidebarNav/index'
import TopNav from './components/TopNav';

import { Feed } from './container/index';
import { NotFound } from './container/index';



function App() {
  return (
    <div id='App' className='flex'>

      <TopNav />  

      <SideBar />

      <Routes>
        <Route path={'/'} element={<Feed />}/>
        <Route path={'*'} element={<NotFound />}/>
      </Routes>

    </div>
  );
};

export default App;
